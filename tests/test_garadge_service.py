import sys
import unittest
from pathlib import Path
from unittest.mock import Mock

from unittest.mock import MagicMock

from src.garage_service import FreeGarageNotFound, GarageService
from src.car import Car


class GarageCleaningService:
    def clean(self, garage):
        pass


class GarageServiceTest(unittest.TestCase):
    def setUp(self) -> None:
        self.garage_cleaning_service = GarageCleaningService()
        self.garage_service = GarageService(self.garage_cleaning_service)

    def park_cars_in_all_garages(self):
        for _ in self.garage_service.SECURE_GARAGES:
            simple_car = Car("Toyota", "Prius", 2015, True)
            self.garage_service.register_in_garage(simple_car)

        for _ in self.garage_service.SIMPLE_GARAGES:
            simple_car = Car("Toyota1", "Prius1", 2016, False)
            self.garage_service.register_in_garage(simple_car)

    def test_when_car_parked_garage_cleaned(self):
        self.garage_cleaning_service.clean = MagicMock(side_effect=self.garage_cleaning_service.clean)
        simple_car = Car("Toyota", "Prius", 2015, False)
        self.garage_service.register_in_garage(simple_car)
        self.garage_cleaning_service.clean.assert_called()

    def test_when_classic_car_parked_to_increased_security_garage(self):
        classic_car = Car("Toyota", "Prius", 2015, True)
        garage_where_car_is_parked = self.garage_service.register_in_garage(
            classic_car)
        self.assertTrue(
            garage_where_car_is_parked in GarageService.SECURE_GARAGES)

    def test_when_not_classic_car_parked_to_usual_garage(self):
        simple_car = Car("Toyota", "Prius", 2015, False)
        garage_where_car_is_parked = self.garage_service.register_in_garage(
            simple_car)
        self.assertTrue(
            garage_where_car_is_parked in GarageService.SIMPLE_GARAGES)

    def test_when_all_garages_have_car_then_error(self):
        self.park_cars_in_all_garages()
        simple_car = Car("Toyota", "Prius", 2015, True)
        with self.assertRaises(FreeGarageNotFound):
            self.garage_service.register_in_garage(simple_car)
